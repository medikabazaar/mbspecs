# Image Search Engine

Website browsing on mobile is problematic, browsing hundreds of products with very basic categorisation, typing the name of the product. The journey from ‘seeing’ a product to buying a product becomes a friction-filled path that often leads to no-purchase at all

Visual search will be able to shorten the path to action as consumers search for products, shoppers will land directly on the product page they were looking for. This  will increase product discovery accuracy as well as conversion to purchase by combining text-based and visual search. 

This visual discovery engine powering these services, and many more will significantly improve engagement in both search and recommendation tasks. 

A user uploads an image by clicking picture of a product to see more details about the same, The most similar product images will be displayed, the user can then click to visit the associated web link; gaining better insights into customer motivations and, more importantly, habits will be used to provide a more tailored experience. 

## Rquirements ##
Python 3+,
Windows or macOS or Linux

# Installation: #

Start by creating vertual environment

## Libraries
```bash
pip install -r requirements.txt 
```

## How to Run the API

```python
run server.py
```
## Following are the 6 APIs ##

1. Updating Product Description Data with Image text: Enchances the Description data which is stored as a word vectored model in a pickle file
2. Image Augmentation: Multiply images by various image data augmentation methods
3. Image Feature Model Creation: Extract features from the augmented images and store them as a feature vector model in a pickle file
4. Similar Products on the basis of Image: Returns 'N' no. of similar products after matching the fecture vector model of the uploaded image and the already stored feature vector model
5. Text Extraction of Image: The API extracts all the texts from the uploaded image and returns the same
6. Similar Products on the basis of text:  Returns 'N' no. of similar products after matching the word vector model of the uploaded image and the already stored word vector model

