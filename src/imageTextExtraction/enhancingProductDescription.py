import os
from flask import Flask, request
from config import config
from config import loggingConfig

logger = loggingConfig.get_logger('enhancingProductDescription')

app = Flask(__name__)


@app.route('/v1/enhanceProductDescription', methods=["GET", "POST"])
def updateProductDescription():
    return "work to be done"
