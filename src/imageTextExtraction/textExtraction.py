from PIL import Image
import pytesseract
import os
from flask import Flask, request
from werkzeug.utils import secure_filename
from config import config
import cv2
from config import loggingConfig

logger = loggingConfig.get_logger('textExtraction')

app = Flask(__name__)

pytesseract.pytesseract.tesseract_cmd = r'C:\Users\Manil Tongya\AppData\Local\Programs\Tesseract-OCR\tesseract.exe'


def extractText(imagePath):
    logger.info("Extracting Text from Image")
    img = Image.open(imagePath)
    if img.format in "".join(config.validImageFormats):
        image = cv2.imread(imagePath)
        textFromImage = pytesseract.image_to_string(image, lang="eng")
        if len(textFromImage) == 1:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            ret, thresh1 = cv2.threshold(gray, 110, 275, cv2.THRESH_BINARY)
            textFromImage = pytesseract.image_to_string(thresh1)
            return textFromImage
        return textFromImage
    logger.error("No text extracted from Image")
    return False


@app.route('/v1/textExtraction', methods=["GET", "POST"])
def extractTextFromImage():
    if request.method == "POST":
        file = request.files['uploadImage']
        if file:
            filename = secure_filename(file.filename)
            imagePath = os.path.join(config.saveFilePathForImage, filename)
            file.save(imagePath)
            textFromImage = extractText(imagePath)
            return textFromImage
    return False
