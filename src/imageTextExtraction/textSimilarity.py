import os
from flask import Flask, request
from config import config
from werkzeug.utils import secure_filename
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from config import loggingConfig
from src.imageTextExtraction.textExtraction import extractText
from config import loggingConfig

logger = loggingConfig.get_logger('textSimilarity')
app = Flask(__name__)


def getTopSimilarityProducts(imagePath):
    textFromImage = extractText(imagePath)
    df = pd.read_excel(config.descriptionData)
    descriptionListData = df['Description'].tolist()

    # Finding cosine similarity
    logger.info("Getting cosine similarity between description data and image text")
    logger.info("Getting cosine similarity between description data and image text")
    tgt_string = textFromImage
    string_list = descriptionListData
    tfidf_vectorizer = TfidfVectorizer()
    sparse_matrix = tfidf_vectorizer.fit_transform(string_list)
    doc_term_matrix = sparse_matrix.toarray()
    tgt_transform = tfidf_vectorizer.transform([tgt_string]).toarray()
    tgt_cosine = cosine_similarity(doc_term_matrix, tgt_transform)

    # Setting threshold to find best matched description
    dataframe = pd.DataFrame(index=string_list, data=tgt_cosine, columns=[tgt_string])
    cosineSimilarityScore = dataframe.values.tolist()
    finalResult = []
    for i in range(len(cosineSimilarityScore)):
        if cosineSimilarityScore[i][0] > 0:
            finalResult.append([cosineSimilarityScore[i][0], df.loc[i]["SKU ID"], df.loc[i]["Product Name"]])
    finalResult = sorted(finalResult, key=lambda x: x[0])[::-1]
    logger.info("Returning most similar products")
    return finalResult


@app.route('/v1/textSimilarity', methods=["GET", "POST"])
def getSimilarity():
    logger.debug("Method type is " + request.method)
    if request.method == "POST":
        file = request.files['uploadImage']
        if file:
            filename = secure_filename(file.filename)
            logger.debug("Uploaded filename is " + filename)
            imagePath = os.path.join(config.saveFilePathForImage, filename)
            file.save(imagePath)
            logger.info("Input image is saved at location -> " + imagePath)
            topSimilarProducts = getTopSimilarityProducts(imagePath)
            return topSimilarProducts
    return False
