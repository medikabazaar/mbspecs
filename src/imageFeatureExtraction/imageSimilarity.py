from flask import Flask, request
from config import config
import numpy as np
import joblib as pickle
import tensorflow
import tensorflow as tf
from tensorflow.keras.preprocessing import image
# from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
# from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from sklearn.neighbors import NearestNeighbors
from tensorflow import keras
import os
import requests
from werkzeug.utils import secure_filename
from datetime import datetime
from config import loggingConfig

logger = loggingConfig.get_logger('imageSimilarity')

tensorflow.get_logger().setLevel('ERROR')
neighbors = NearestNeighbors(n_neighbors=config.topSimilerImages, algorithm='ball_tree', metric='euclidean')
neighbors.fit(config.feature_list)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
app = Flask(__name__)

graph = tf.get_default_graph()


def getTopSimilarProducts(imagePath):
    model = VGG16(weights='imagenet', include_top=False, input_shape=(config.img_size, config.img_size, 3),
                  pooling='max')
    logger.info("Model has been loaded")
    input_shape = (config.img_size, config.img_size, 3)
    img = image.load_img(imagePath, target_size=(input_shape[0], input_shape[1]))
    img_array = image.img_to_array(img)
    expanded_img_array = np.expand_dims(img_array, axis=0)
    preprocessed_img = preprocess_input(expanded_img_array)
    test_img_features = model.predict(preprocessed_img, batch_size=1)
    logger.info("Input image features has been extracted")
    distances, indices = neighbors.kneighbors(test_img_features)
    distanceOfIndices = []
    for i in range(len(indices[0])):
        distanceOfIndices.append([distances[0][i], indices[0][i]])
    images_name = imageSimilarity(config.filenames, distanceOfIndices)
    return images_name


def imageSimilarity(filenames, distanceOfIndices):
    logger.info("Finding similar products on the basis of nearest neighbour")
    images_name = []
    distanceOfIndices = sorted(distanceOfIndices, key=lambda x: x[0])
    for index in distanceOfIndices:
        productName = filenames[index[1]][::-1].split("/")[0][::-1].split("_")[0]
        images_name.append(productName)
    images_name = list(dict.fromkeys(images_name))
    logger.info("Returning most similar products")
    return images_name


@app.route('/v1/imageSimilarity', methods=["POST"])
def getSimilarity():
    logger.info("Method type is " + request.method)
    if request.method == "POST":
        file = request.files['uploadImage']
        if file:
            filename = secure_filename(file.filename)
            logger.info("Upload filename is" + filename)
            imagePath = os.path.join(config.saveFilePathForImage, filename)
            file.save(imagePath)
            logger.info("Input image is saved at location -> " + imagePath)
            getSimilarImages = getTopSimilarProducts(imagePath)
            return getSimilarImages
    return False


def testImageSimilarity():
    path_img = 'test.jpg'
    url = 'http://localhost:5000/v1/imageSimilarity'
    with open(path_img, 'rb') as img:
        name_img = os.path.basename(path_img)
        files = {'uploadImage': (name_img, img, 'multipart/form-data', {'Expires': '0'})}
        with requests.Session() as s:
            r = s.post(url, files=files)
            print(r.content)
