from flask import Flask, request
from config import config
# from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import math
import joblib as pickle
from config import loggingConfig

logger = loggingConfig.get_logger('createFeatureModel')

app = Flask(__name__)


@app.route('/v1/imageFeatureCreation', methods=["GET", "POST"])
def createModel():
    if request.method == "GET":
        model = VGG16(weights='imagenet', include_top=False, input_shape=(config.img_size, config.img_size, 3),
                      pooling='max')
        img_gen = ImageDataGenerator(preprocessing_function=preprocess_input)
        print("datagen has started")
        datagen = img_gen.flow_from_directory(config.MBImagePath,
                                              target_size=(config.img_size, config.img_size),
                                              batch_size=config.batch_size,
                                              class_mode=None,
                                              shuffle=False)
        print("datagen has been completed")
        num_images = len(datagen.filenames)
        num_epochs = int(math.ceil(num_images / config.batch_size))
        print("num_images=", num_images, ", num_epochs=", num_epochs)

        feature_list = model.predict_generator(datagen, num_epochs)
        print("feature_list completed")
        filenames = [config.inputFilePath + '/' + s for s in datagen.filenames]
        # Store extracted features
        print("Starting to create models")
        pickle.dump(filenames, open(config.store_filenames + '_VGG.pickle', 'wb'))
        print("filenames has been completed")
        pickle.dump(feature_list, open(config.store_features + '_VGG.pickle', 'wb'))
        print("features has been completed")

        return True
    return False

