from config import config
from flask import Flask, request
import os
import tensorflow.compat.v1 as tf
import matplotlib
import matplotlib.image as mpimg
import numpy as np
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg
from config import loggingConfig

logger = loggingConfig.get_logger('imageAugmentation')
app = Flask(__name__)
tf.enable_eager_execution()


@app.route('/v1/textSimilarity', methods=["GET", "POST"])
def generateAugmentedImages():
    if request.method == "GET":
        listOfFolders = os.listdir(config.MBImagePath)
        listOfFolders = [config.MBImagePath + x for x in listOfFolders]
        cnt = 0
        for folder in listOfFolders:
            listOfImages = os.listdir(folder)
            pathOfImages = []
            productSKUID = ""
            try:
                for img in listOfImages:
                    if '.DS_Store' not in img:
                        pathOfImages.append(folder + "\\" + img)
                        productSKUID = img.split('_')[0]
                X_imgs = tf_resize_images(pathOfImages)
                aug = ImageDataGenerator(rotation_range=30, zoom_range=0.15, width_shift_range=0.2, height_shift_range=0.2,
                                         shear_range=0.15, horizontal_flip=True, fill_mode="nearest")
                total = 0
                logger.info("productSKUID = " + productSKUID)
                cnt += 1
                imageGen = aug.flow(X_imgs, batch_size=1, save_to_dir=folder+"\\",
                                    save_prefix=productSKUID, save_format="jpg")
                # Generate 100 images
                for X_imgs in imageGen:
                    total += 1
                    if total == 100:
                        break
                logger.info("Success")
            except:
                pass

        return True
    return False


def tf_resize_images(X_img_file_paths):
    X_data = []
    tf.compat.v1.reset_default_graph()
    X = tf.placeholder(tf.float32, (None, None, 3))
    tf_img = tf.image.resize_images(X, (config.img_size, config.img_size), tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        # Each image is resized individually as different image may be of different size.
        for index, file_path in enumerate(X_img_file_paths):
            img = mpimg.imread(file_path)[:, :, :3]
            resized_img = sess.run(tf_img, feed_dict={X: img})
            X_data.append(resized_img)

    X_data = np.array(X_data, dtype=np.float32)
    return X_data
