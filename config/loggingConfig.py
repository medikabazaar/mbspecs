import logging
from logging.handlers import RotatingFileHandler
from logging.handlers import TimedRotatingFileHandler


def get_logger(name):
    log_format = '%(asctime)s [%(threadName)s] [%(levelname)s] %(name)s: %(message)s'
    handler = TimedRotatingFileHandler(filename='logs/SearchEngine.log',
                                       when="midnight",
                                       delay=0
                                       )
    handler.suffix = "%Y%m%d"
    '''
    rfh = RotatingFileHandler(
        filename='logs/SearchEngine.log',
        mode='a',
        maxBytes=5 * 1024 * 1024,
        backupCount=10,
        encoding=None,
        delay=0
    )
    '''
    logging.basicConfig(level=logging.INFO,
                        format=log_format,
                        handlers=[
                            handler
                        ])

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger(name).addHandler(console)
    return logging.getLogger(name)
