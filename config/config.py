import os
import warnings
import os
import joblib as pickle
from sklearn.neighbors import NearestNeighbors
import tensorflow as tf

from tensorflow.keras.applications.resnet50 import ResNet50

tf.get_logger().setLevel('ERROR')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.filterwarnings('ignore')

saveFilePathForImage = "E:\\MBLens\\uploadFiles\\"
validImageFormats = ["JPEG", "PNG", "PPM"]

descriptionData = "E:\\MBLens\\files\\Product_Description.xlsx"

MBImagePath = "E:\\MBLens\\files\\tests\\"
AugmentedImagePath = "E:\\MBLens\\files\\augmentedImages\\"
inputFilePath = "E:/MBLens/files/augmentedImages"
# inputFilePath = "E:/MBLens/files/tests"
img_size = 224
batch_size = 64
# path to store the filenames and extracted features
store_filenames = 'E:\\MBLens\\models\\MB_Product_filenames'
store_features = 'E:\\MBLens\\models\\MB_Product_features'
topSimilerImages = 100

filenames = pickle.load(open(store_filenames + "_VGG.pickle", 'rb'))
feature_list = pickle.load(open(store_features + "_VGG.pickle", 'rb'))
