import requests
import os
import unittest


class TestCases(unittest.TestCase):

    def test1(self):
        self.assertEqual(0, 0)

    def testImageSimilarity(self):
        path_img = 'test.jpg'
        url = 'http://localhost:5000/v1/imageSimilarity'
        with open(path_img, 'rb') as img:
            name_img = os.path.basename(path_img)
            files = {'uploadImage': (name_img, img, 'multipart/form-data', {'Expires': '0'})}
            with requests.Session() as s:
                r = s.post(url, files=files)
                # print(r.content.decode('utf-8'))
                self.assertEqual(r.status_code, 200)

    def testTextSimilarity(self):
        path_img = 'test.jpg'
        url = 'http://localhost:5000/v1/textSimilarity'
        with open(path_img, 'rb') as img:
            name_img = os.path.basename(path_img)
            files = {'uploadImage': (name_img, img, 'multipart/form-data', {'Expires': '0'})}
            with requests.Session() as s:
                r = s.post(url, files=files)
                # print(r.content.decode('utf-8'))
                self.assertEqual(r.status_code, 200)

    def testTextExtractionFromImage(self):
        path_img = 'test.jpg'
        url = 'http://localhost:5000/v1/textExtraction'
        with open(path_img, 'rb') as img:
            name_img = os.path.basename(path_img)
            files = {'uploadImage': (name_img, img, 'multipart/form-data', {'Expires': '0'})}
            with requests.Session() as s:
                r = s.post(url, files=files)
                # print(r.content.decode('utf-8'))
                self.assertEqual(r.status_code, 200)


if __name__ == '__main__':
    unittest.main()
