import connexion
import os
from config import loggingConfig
from datetime import datetime

# Log file name
logFile = 'logs/SearchEngine.log'
currTime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
oldLogFile = 'logs/SearchEngine_' + currTime + '.log'

# if os.path.isfile(logFile):
#    os.rename(logFile, oldLogFile)

# Create the application instance
app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
app.add_api('swagger.yml')
logger = loggingConfig.get_logger('server')

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

